variable "tags" {
    type = "map"
}

variable "environment" {
}

variable "region" {
}


variable "vpc_id" {
}

variable "public_domain" {
}

variable "path-prefix" {
  
}

variable "feature" {
  
}

variable "priority_prefix" {
}
