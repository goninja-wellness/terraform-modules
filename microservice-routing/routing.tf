resource "aws_alb_listener_rule" "host_based_routing" {
  listener_arn = "${data.aws_alb_listener.http_listener.arn}"
  priority     = "${var.priority_prefix}1"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${aws_route53_record.microservice_loadbalancer.name}"]
  }
}

resource "aws_alb_listener_rule" "path_based_routing" {
  listener_arn = "${data.aws_alb_listener.http_listener.arn}"
  priority     = "${var.priority_prefix}2"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
  }
  
  condition {
    field  = "path-pattern"
    values = ["/${var.path-prefix}/*"]
  }
}
resource "aws_alb_listener_rule" "path_based_routing2" {
  listener_arn = "${data.aws_alb_listener.http_listener.arn}"
  priority     = "${var.priority_prefix}3"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
  }
  
  condition {
    field  = "path-pattern"
    values = ["/${var.path-prefix}"]
  }
}

resource "aws_alb_target_group" "target_group" {
  name     = "${var.environment}-${var.feature}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
  tags     = "${local.common_tags}"
  health_check {
    path   = "/health"
  }
}

resource "aws_route53_record" "microservice_loadbalancer" {
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  name    = "${var.path-prefix}.${var.environment}.${var.public_domain}"
  type    = "A"
  alias{
    name = "${data.aws_alb.load_balancer.dns_name}"
    zone_id = "${data.aws_alb.load_balancer.zone_id}"
    evaluate_target_health = true
  }
}
