output "target_group_arn" {
    value = "${aws_alb_target_group.target_group.arn}"
}

output "http_listener_arn" {
  value = "${data.aws_alb_listener.http_listener.arn}"
}
