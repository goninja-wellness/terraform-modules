data "terraform_remote_state" "shared_cluster" {
  backend = "s3"
  config = {
    bucket = "terraform.dev.goninjawellness.co.uk"
    key    = "state/${var.environment}/goninja/wellness/shared-cluster/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_alb" "load_balancer" {
  arn  = "${data.terraform_remote_state.shared_cluster.outputs.alb}"
}

data "aws_alb_listener" "http_listener" {
  arn = "${data.terraform_remote_state.shared_cluster.outputs.alb_listener}"
}

data "aws_route53_zone" "public" {
  name         = "${var.public_domain}."
}
