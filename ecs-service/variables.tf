variable "tags" {
    type = "map"
}

variable "environment" {
}

variable "region" {
}

variable "feature" {
  
}

variable "target_group_arn" {
  
}

variable "cluster_id" {
  
}

variable "task_policy_json" {
  
}
