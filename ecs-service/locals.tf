
locals {
  common_tags = "${merge(
    map(
      "gn:product", "goninja-wellness",
      "gn:service", "goals-microservice",
      "gn:feature", "${var.feature}",
      "gn:environment", "${var.environment}",
      "environment", "${var.environment}",
    ),
    var.tags
  )}"
}

