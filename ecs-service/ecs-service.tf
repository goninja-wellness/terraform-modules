resource "aws_cloudwatch_log_group" "log_group" {
    name              = "${var.environment}-${var.feature}"
    retention_in_days = 1
    tags             = "${local.common_tags}"
  }
  
data "aws_ecr_repository" "service_repo" {
  name = "${var.feature}"
}

resource "aws_ecs_task_definition" "task" {
  family = "${var.environment}-${var.feature}"
  task_role_arn = "${aws_iam_role.task_role.arn}"
  container_definitions = <<EOF
[
  {
    "name": "${var.environment}-${var.feature}",
    "image": "${data.aws_ecr_repository.service_repo.repository_url}:latest",
    "cpu": 0,
    "memory": 128,
    "portMappings": [{
      "containerPort": 80
    }],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-west-1",
        "awslogs-group": "${aws_cloudwatch_log_group.log_group.name}",
        "awslogs-stream-prefix": "complete-ecs"
      }
    }
  }
]
EOF
}

resource "aws_ecs_service" "service" {
  name            = "${var.environment}-${var.feature}"
  cluster         = "${var.cluster_id}"
  task_definition = "${aws_ecs_task_definition.task.id}"

  desired_count = 1

  load_balancer {
    container_name = "${var.environment}-${var.feature}"
    container_port = "80"
    target_group_arn = "${var.target_group_arn}"
  }
}

resource "aws_iam_role" "task_role" {
  name               = "${var.environment}-${var.feature}"
  path               = "/system/"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_assume_role_policy.json}"
}

data "aws_iam_policy_document" "ecs_assume_role_policy" {
  statement {
    actions       = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}
resource "aws_iam_policy" "task_policy" {
  name   = "${var.environment}-${var.feature}"
  path   = "/"
  policy = "${var.task_policy_json}"
}

resource "aws_iam_policy_attachment" "task_policy_attachment" {
  name       = "${var.environment}-${var.feature}-attachment"
  policy_arn = "${aws_iam_policy.task_policy.arn}"
  roles      = ["${aws_iam_role.task_role.name}"]
}